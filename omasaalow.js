var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    port = process.env.PORT || 9999,
    morgan = require('morgan'),             // log requests to the console (express4)
    bodyParser = require('body-parser'),    // pull information from HTML POST (express4)
    email = require(__dirname + '/app/controller/email'),
    geo = require(__dirname + '/app/controller/geolocation');


    var chatdata = [];



app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended': 'true'}));            // parse application/x-www-form-urlencoded
app.use(express.static(__dirname + '/public'));

function removeWWW(req, res, next){
    if (req.headers.host.match(/^www/) !== null ) {
        res.redirect('http://' + req.headers.host.replace(/^www\./, '') + req.url);
    } else {
        next();
    }
}
app.use(removeWWW);


// EXPRESS ROUTEN
app.get('/garage', function(req, res){

    var timestamp = new Date();

    // get client ip and do geo location request
    var geoData = geo.getLocationForIp(req.connection.remoteAddress, function(err, res){
        if(!err) {
            var reqBody = {
                inhalt : "Es hat jemand die Station Garage aufgerufen.\n\n" + "Zeitpunkt:" + timestamp.toLocaleString(),
                betreff : "Station Garage...",
                email : "system@oma-saalow.de",
                handy : 110,
                geoData : res
            };

            email.send(reqBody, function(err, res){
                if(err) {
                    console.error(err);
                } else {
                    console.info(res);
                }
            });
        }
    });

    res.render('pages/garage');
});

//app.get('/chat', function(req, res){
//    res.render('pages/chat');
//});

app.get('/hof', function(req, res){
    res.render('pages/hof');
});

app.get('/banner', function(req, res){
    res.render('pages/banner.ejs');
});

app.get('/ueber', function(req, res){
    res.render('pages/ueber');
});

app.get('/kontakt', function(req, res){
    res.render('pages/kontakt');
});

app.post('/email', function (req, res) {

    email.send(req.body, function(err, res){
        if(err) {
            console.error(err);
        } else {
            console.info(res);
        }
    });

    res.render('pages/email', {"name" : req.body.email});
});

app.post('/antwort', function(req, res){

    var timestamp = new Date();

    // get client ip and do geo location request
    var geoData = geo.getLocationForIp(req.connection.remoteAddress, function(err, res){
        if(!err) {
            var reqBody = {
                inhalt : "Es hat jemand das Willkommen Oma Saalow Frage Formular ausgefüllt und abgeschickt.\n\n" + req.body.antwort + "\n\n" + "Zeitpunkt:" + timestamp.toLocaleString(),
                betreff : "Station Willkommen...",
                email : "system@oma-saalow.de",
                handy : 110,
                geoData : res
            };

            email.send(reqBody, function(err, res){
                if(err) {
                    console.error(err);
                } else {
                    console.info(res);
                }
            });
        }
    });

    res.render('pages/antwort', {"antwort" : req.body.antwort.replace(/\s/g, '')});
});

app.get('/', function(req, res){
    res.render('pages/index');
});

app.get('*', function (req, res) {
    res.status(404).send('Ressource not found ...');
});


// start http server, not express directly, because socket.io is bound to http server
server.listen(port);

console.log('started Oma Saalow Webapp - listening on port 9999 ...');

// Websocket
io.sockets.on('connection', function (socket) {
    // der Client ist verbunden
    socket.emit('chat', { zeit: new Date(), text: 'Willkommen im Oma Saalow Hilfe Chat!' });

    for(item in chatdata){
        console.log(chatdata[item]);
        socket.emit('chat', chatdata[item]);
    }

    // wenn ein Benutzer einen Text senden
    socket.on('chat', function (data) {
        // so wird dieser Text an alle anderen Benutzer gesendet
        io.sockets.emit('chat', { zeit: new Date(), name: data.name || 'Anonym', text: data.text });
        chatdata.push({ zeit: new Date(), name: data.name || 'Anonym', text: data.text });
    });
});