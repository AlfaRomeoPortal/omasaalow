var http = require('http');

module.exports = {

    getLocationForIp : function(ipAdress, callback){
        var serverURL = 'http://ipinfo.io/' + ipAdress + '/json';

        http.get(serverURL, function(res) {

            res.on("data", function(chunk) {
                console.log("BODY: " + chunk);

                callback(null, chunk.toString());
            });

        }).on('error', function(e) {
            console.log("Got error: " + e.message);
            callback(e, null);
        });
    }
}