var nodemailer = require('nodemailer');

module.exports.send = function(postBody, callback){
    console.log('body: ', postBody);
    var _text = 'Es kam eine neue Anfrage von: \n' +
        postBody.email + '\n' +
        postBody.handy + '\n\n' +
        postBody.geoData + '\n\n' +
            'Folgender Inhalt wurde übermittelt: \n' +
        postBody.inhalt;



    // create reusable transporter object using SMTP transport
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'oma.saalow@gmail.com',
            pass: 'xyz'
        }
    });

    var mailOptions = {
        from: postBody.email, // sender address
        to: 'oma.saalow@gmail.com', // list of receivers
        subject: postBody.betreff, // Subject line
        text: _text, // plaintext body
        html: '' // html body
    };


    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            callback(error, null);
        }else{
            callback(null, 'Message sent: ' + info.response);
        }
    });
}